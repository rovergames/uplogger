# UpLogger

_UpLogger_ allows you to override the global console to make the log better !

## Installation

```bash
$ npm i uplogger
```

## Init

```js
const UpLogger = require('uplogger');

new UpLogger();
```

## Log with UpLogger

_UpLogger_ extends default Console object so you can use all the default method to log.

## Documentation and more infos

[uplogger.gitlab.io](https://rovergames.gitlab.io/uplogger-doc/)

## License

GPL-3.0
