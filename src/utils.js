/* eslint-disable no-confusing-arrow */
const twoDigits = number => number >= 10 ? number : `0${number}`;

export default { twoDigits };
