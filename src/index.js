import { Console } from 'console';
import os from 'os';
import path from 'path';
import util from 'util';

import stackTrace from 'stack-trace';

import LogLevel from './LogLevel';
import { twoDigits } from './utils';

const defaultConfig = {
    stdout: process.stdout,
    stderr: process.stderr,
    useGlobal: true,
    separator: '->',
    logLevel: {},
};

class UpLogger extends Console {
    constructor(cfg = {}) {
        const config = Object.assign({}, defaultConfig, cfg);

        super(config.stdout, config.stderr);

        this.config = config;

        if (this.config.useGlobal) {
            delete global.console;
            global.console = this;
        }

        this.logLevel = Object.assign({}, LogLevel, this.config.logLevel);
    }

    write(id, msg, ...args) {
        const logLevelKeys = Object.keys(this.logLevel);
        const logLvl = logLevelKeys.filter(el => this.logLevel[el].id === id);

        this._write(this.logLevel[logLvl], this.config.stdout, msg, args);
    }

    success(msg, ...args) {
        this._write(this.logLevel.SUCCESS, this.config.stdout, msg, args);
    }

    log(msg, ...args) {
        this._write(this.logLevel.INFO, this.config.stdout, msg, args);
    }

    info(msg, ...args) {
        this.log(msg, ...args);
    }

    debug(msg, ...args) {
        this._write(this.logLevel.DEBUG, this.config.stdout, msg, args);
    }

    warn(msg, ...args) {
        this._write(this.logLevel.WARN, this.config.stderr, msg, args);
    }

    error(msg, ...args) {
        this._write(this.logLevel.ERROR, this.config.stderr, msg, args);
    }

    _write(logLvl, stream, msg, args = []) {
        if (typeof logLvl !== 'object' && Object.hasOwnProperty.call(logLvl, 'id') && Object.hasOwnProperty.call(logLvl, 'label') && Object.hasOwnProperty.call(logLvl, 'lblColor') && Object.hasOwnProperty.call(logLvl, 'msgColor') && Object.hasOwnProperty.call(logLvl, 'separatorColor') && Object.hasOwnProperty.call(logLvl, 'template')) {
            logLvl = this.logLevel.ERROR;
            msg = 'LogLevel must be an object with id, label, color (chalk function) and template propeties';
        }

        const date = new Date();
        const dateString = `${twoDigits(date.getHours())}:${twoDigits(date.getMinutes())}:${twoDigits(date.getSeconds())}`;

        const filteredStack = stackTrace.get().filter(el => el.getFileName() !== __filename && el.getFileName() !== 'console.js');

        const trace = filteredStack[0];
        const filename = path.basename(trace.getFileName());
        const line = trace.getLineNumber();
        const column = trace.getColumnNumber();

        let formattedMsg = logLvl.template;

        formattedMsg = formattedMsg.replace('#D', dateString);
        formattedMsg = formattedMsg.replace('#N', logLvl.lblColor(logLvl.label));
        formattedMsg = formattedMsg.replace('#F', filename);
        formattedMsg = formattedMsg.replace('#L', line);
        formattedMsg = formattedMsg.replace('#C', column);
        formattedMsg = formattedMsg.replace('#S', logLvl.separatorColor(this.config.separator));
        formattedMsg = formattedMsg.replace('#M', logLvl.msgColor(util.format(msg, ...args)));

        stream.write(`${formattedMsg}${os.EOL}`);
    }
}

export default UpLogger;
