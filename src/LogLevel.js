import chalk from 'chalk';

export default {
    SUCCESS: { id: 0, label: 'SUCCESS', lblColor: chalk.bgGreen, msgColor: chalk.green, separatorColor: chalk.white, template: '#D: #N #S #M' },
    INFO: { id: 1, label: 'INFO', lblColor: chalk.white, msgColor: chalk.white, separatorColor: chalk.white, template: '#D: #N #S #M' },
    DEBUG: { id: 2, label: 'DEBUG', lblColor: chalk.bgCyan, msgColor: chalk.cyan, separatorColor: chalk.white, template: '#D: #N: #F:#L:#C #S #M' },
    WARN: { id: 3, label: 'WARN', lblColor: chalk.bgYellow, msgColor: chalk.yellow, separatorColor: chalk.white, template: '#D: #N: #F:#L:#C #S #M' },
    ERROR: { id: 4, label: 'ERROR', lblColor: chalk.bgRed, msgColor: chalk.red, separatorColor: chalk.white, template: '#D: #N: #F:#L:#C #S #M' },
};
